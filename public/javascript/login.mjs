const username = sessionStorage.getItem("username");

if (username) {
  window.location.replace("/game");
}

export let users = [];

const socket = io("", { query: { username } });

const submitButton = document.getElementById("submit-button");
const input = document.getElementById("username-input");

const getInputValue = () => input.value;

const onClickSubmitButton = () => {
  const inputValue = getInputValue();
  if (!inputValue) {
    return;
  }
  if(checkUser(users, inputValue)){
    sessionStorage.setItem("username", inputValue);
    window.location.replace("/game");
  }else {
    alert(`User with name ${username} is already logined!`);
    sessionStorage.removeItem("username", inputValue);
    window.location.replace("/login");
  }
};

const onKeyUp = ev => {
  const enterKeyCode = 13;
  if (ev.keyCode === enterKeyCode) {
    submitButton.click();
  }
};

submitButton.addEventListener("click", onClickSubmitButton);
window.addEventListener("keyup", onKeyUp);

const checkUser = (users, username) => users.indexOf(username) === -1;
socket.on('GET_USERS', (clients) => {users = clients});
