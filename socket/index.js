import * as config from "./config";

let clients = [];

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    clients = findClientsSocket(null, '/');

    socket.emit('GET_GAME_VALUES', config);
    socket.emit('GET_USERS', clients);

    socket.on('disconnect', () => {
      clients.splice(clients.indexOf(clients.find(user => user.name === username)), 1);
      console.log(clients);
      console.log(`${socket.handshake.query.username} disconnected`);
      socket.on('UPDATE_USERS_DATA', clients => {
        socket.emit('GET_USERS', clients);
        socket.broadcast.emit('GET_USERS', clients);
      }
    );
    });

    socket.on('UPDATE_USERS_DATA', clients => {
        socket.emit('GET_USERS', clients);
        socket.broadcast.emit('GET_USERS', clients);
      }
    );

    socket.on('ALL_READY', () => {
      let id = Math.floor(Math.random() * Math.floor(6));
      socket.emit('START_COUNTDOWN', id);
      socket.broadcast.emit('START_COUNTDOWN', id);
    });

    socket.on('TIME_OVER', () => {
      //socket.emit('FINISH_GAME', 'Time is over!', 'Let\'s try again!');
      socket.broadcast.emit('FINISH_GAME', 'Time is over!', 'Let\'s try again!');
    });

    socket.on('GAME_OVER', (winner, usersList) => {
      usersList.sort((a,b) => (a.progressIndex > b.progressIndex) ? -1 : ((b.progressIndex > a.progressIndex) ? 1 : 0))

      let ratingText = '';
      for (let i = 0; i < usersList.length; i++) {
        ratingText += `#${i + 1}: ${usersList[i].name}\n`
      }

      socket.emit('FINISH_GAME', 'Winner is ' + winner.name, ratingText);
      socket.broadcast.emit('FINISH_GAME', 'Winner is ' + winner.name, ratingText);
    });

  });

  const findClientsSocket = (roomId, namespace) => {
    let res = []
    let ns = io.of(namespace || "/");

    if (ns) {
      for (let id in ns.connected) {
        if (roomId) {
          let index = ns.connected[id].rooms.indexOf(roomId);
          if (index !== -1) {
            res.push({
              name: ns.connected[id].handshake.query.username,
              status: false,
              progressIndex: 0
            });
          }
        } else {
          res.push({
            name: ns.connected[id].handshake.query.username,
            status: false,
            progressIndex: 0
          });
        }
      }
    }
    return res;
  }
};