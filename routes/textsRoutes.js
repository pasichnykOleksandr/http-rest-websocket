import { Router } from "express";
import path from "path";
import texts from "../data"

const router = Router();

router
  .get("/", (req, res) => {
    res.send(texts);
  });

export default router;
