import { createElement, addClass, removeClass } from './helper.mjs';
import { showWinnerModal } from './modal/winner.mjs'

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

const playerListField = document.querySelector('.players-list');
const readyBtn = document.querySelector('.ready-btn');
const disconnectBtn = document.querySelector('.disconnect-btn');
const readyBtnField = document.querySelector('.game-ready-btn-field');
const gameTextField = document.querySelector('.game-text-field');
const countdownField = document.querySelector('.countdown-field');
const countdownNumber = document.querySelector('.countdown-number');
const gameTime = document.querySelector('.game-timer');
const gameTimerNumber = document.querySelector('.game-timer-number');
const gameText = document.querySelector('.game-text');

let SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME;
let usersList = [];

const getValues = (configs) => {
  SECONDS_TIMER_BEFORE_START_GAME = configs.SECONDS_TIMER_BEFORE_START_GAME;
  SECONDS_FOR_GAME = configs.SECONDS_FOR_GAME;
};

const getText = (id) => {
  let xhr = new XMLHttpRequest();
  xhr.open('GET', `/game/texts`);
  xhr.send();

  xhr.onload = function() {
    let text = JSON.parse(xhr.response).texts[id];
    gameText.textContent = text;
  };

  xhr.onerror = function() {
    gameText.textContent = '404: Text not found!';
  };

};

const renderUsers = (users) => {
  playerListField.textContent = '';
  users.forEach(user => {
    if(user){
      let item = createElement({
        tagName: "div",
        className: "player-item",
        attributes: {"data-username": user.name}
      });
      let playerInfo = createElement({
        tagName: "div",
        className: "player-info",
        attributes: {}
      });
      let statusCircle = createElement({
        tagName: "div",
        className: "status-circle",
        attributes: {}
      });
      addClass(statusCircle, user.status ? 'active-circle' : 'inactive-circle');
      let playerName = createElement({
        tagName: "p",
        className: "player-name",
        attributes: {}
      });
      playerName.textContent = user.name;
      let playerProgressBar = createElement({
        tagName: "div",
        className: "player-progress-bar",
        attributes: {}
      });
      let progressLine = createElement({
        tagName: "div",
        className: "progress-line",
        attributes: {}
      });

      progressLine.style.width = `${(user.progressIndex/gameText.textContent.length)*100}%`;

      if(username === user.name){
        addClass(playerName, 'current-player-name');
        addClass(progressLine, 'current-player-progress-line');
      }
  
      playerInfo.append(statusCircle, playerName);
      playerProgressBar.append(progressLine);
      item.append(playerInfo, playerProgressBar);
      playerListField.append(item);

      if(user.progressIndex === gameText.textContent.length){
        progressLine.style.backgroundColor = 'green';
      }
    }
  });
};

const showTextField = () => {
  countdownField.style.display = 'none';
  gameTextField.style.display = 'block';
};

const checkPressedKey = (event) => {
  let user = usersList.find(user => user.name === username)
  if(event.key === gameText.textContent.charAt(user.progressIndex).toLowerCase()){
    window.getSelection().removeAllRanges();
    user.progressIndex++;
    let range = new Range();
    range.setStart(gameText.firstChild, 0);
    range.setEnd(gameText.firstChild, user.progressIndex);
    window.getSelection().addRange(range);
  };
  socket.emit('UPDATE_USERS_DATA', usersList);
  usersList.forEach(user => {
    if(user.progressIndex === gameText.textContent.length){
      socket.emit('GAME_OVER', user, usersList);
    }
  });
};

const startGame = () => {
  startTimer();
  gameTime.style.display = 'block';
  showTextField();
  document.addEventListener('keydown', checkPressedKey);
};

const startCountdown = () => {
  let seconds = SECONDS_TIMER_BEFORE_START_GAME;
  countdownNumber.textContent = seconds;
  disconnectBtn.style.visibility = 'hidden';
  readyBtnField.style.display = 'none';
  countdownField.style.display = 'flex';
  let countdown = setInterval(() => {
    seconds--;
    countdownNumber.textContent = seconds;
  }, 1000);

  setTimeout(() => {
    startGame();
    clearInterval(countdown);
  }, seconds * 1000);
};

const startTimer = () => {
  let seconds = SECONDS_FOR_GAME;
  gameTimerNumber.textContent = seconds + ' second left';
  let timer = setInterval(() => {
    seconds--;
    gameTimerNumber.textContent = seconds === 1 ? seconds + ' second left' : seconds + ' seconds left';
  }, 1000);

  setTimeout(() => {
    clearInterval(timer);
    let isFinished = usersList.find(user => user.progressIndex === gameText.textContent.length);
    if(isFinished) socket.emit('TIME_OVER');
  }, seconds * 1000);
};

const setActiveStatus = () => {
  //startCountdown
  let user = usersList.find(user => user.name === username);
  user.status = !user.status;
  statusSwitcher(user);
  readyBtnSwitcher(user);
  socket.emit('UPDATE_USERS_DATA', usersList);
  if(checkReadyAllUsers()){
    socket.emit('ALL_READY');
  }
};

const statusSwitcher = (user) => {
  let item = document.querySelector(`[data-username=${user.name}]`);
  let circle = item.firstChild.firstChild;
  removeClass(circle, user.status ? 'inactive-circle' : 'active-circle');
  addClass(circle, user.status ? 'active-circle' : 'inactive-circle');
}

const readyBtnSwitcher = (user) => {
  readyBtn.textContent = user.status ? 'Not ready' : 'Ready';
}

const checkReadyAllUsers = () => usersList.every(user => user.status === true);

gameText.addEventListener('mousedown', (event) => event.preventDefault());
readyBtn.addEventListener('click', setActiveStatus);
disconnectBtn.addEventListener('click', socket.emit('disconnect', username));

socket.on('GET_GAME_VALUES', getValues);
socket.on('GET_USERS', users => {
  usersList = users;
  renderUsers(users);
});

socket.on('START_COUNTDOWN', (id) => {
  startCountdown();
  getText(id);
});

socket.on('FINISH_GAME', (headerText, message) => {
  gameTime.style.display = 'none';
  showWinnerModal(headerText, message);
  gameText.removeEventListener('mousedown', (event) => event.preventDefault());
  usersList.forEach(user => {
    user.progressIndex = 0;
    user.status = false;
  });
  //socket.emit('UPDATE_USERS_DATA', usersList);
  //window.location.replace("/game");
});