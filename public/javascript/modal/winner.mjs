import {showModal} from "./modal.mjs";

export function showWinnerModal(headerText, message) {
  let winnerData = document.createElement('div');
  winnerData.classList.add('winner-data');
  
  let messageText = document.createElement('p');
  messageText.classList.add('modal-message');
  messageText.innerHTML = message;

  winnerData.append(messageText);

  const winnerInfo = {
    title: headerText,
    bodyElement: winnerData
  }
  showModal(winnerInfo);
}
